*Config*

To configure GCM / notifications (Firebase) follow the instructions

https://community.openhab.org/t/tutorial-own-openhab-cloud-and-gcm-notifications-firebase/38287

if you need APN, add the following section populated with your values to config.json
```
"apn" : {
      "team": "PB1234567",
      "keyId": "BLABLA1",
      "defaultTopic": "es.spaphone.openhab",
      "signingKey": "certs/aps/AuthKey.p8"
    },
```

To restart service, run in the terminal:
```
$ supervisorctl restart openhab
```
