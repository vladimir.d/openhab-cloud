# Cloudron App

It contains a generic config that can be used to build openHAB Cloud app 

# Build & Install
```
Tag=cloudron-$(date +%s)
```


To build and push image to Gitlab registry:

```
$ docker build --platform linux/amd64 -f Dockerfile . -t registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/openhab-cloud:$Tag && \
    docker push registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/openhab-cloud:$Tag
```


To install app on Cloudron:

```
$ cloudron install --no-wait --server YOUR-CLOUDRON-SERVER --location openhab-cloud --image registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/openhab-cloud:$Tag --token YOUR-GITLAB-TOKEN
```

Update app on Cloudron:

```
$ cloudron update --no-wait --no-backup --server YOUR-CLOUDRON-SERVER --app mosquitto  --image registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/openhab-cloud:$Tag --token YOUR-GITLAB-TOKEN
```

To remove image from registry:

```
$ docker rmi -f registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/openhab-cloud:$Tag
```
