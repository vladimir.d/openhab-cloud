FROM ubuntu:focal

ENV DEBIAN_FRONTEND noninteractive \
	VERSION=v7.10.1 NPM_VERSION=4 \
	NODE_PATH="/app/data/node_modules" \
	OPENHAB_DIR="/app/data/openhab"

RUN sed -i 's/deb-src/# deb-src/' /etc/apt/sources.list

RUN apt-get update && \
    apt-get upgrade -y -f && \
    apt-get install --no-install-recommends --allow-unauthenticated -y -f \
        sudo \
        curl \
        gnupg \
        ca-certificates \
        supervisor \
		net-tools \
		build-essential \
		npm \
		mongodb-clients \
		python \
		git \
        vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN adduser --system --no-create-home --group --disabled-login openhab && \
	git clone https://github.com/openhab/openhab-cloud.git /app/data/openhab && \
	chown -R openhab:openhab /app/data/openhab && \
	mkdir -p /run/logs/openhab && \
	chown -R openhab:openhab /run/logs/openhab && \
	ln -s /run/logs/openhab /app/data/openhab/logs

WORKDIR "/app/data/openhab"

RUN npm install

EXPOSE 3000

# supervisor
ADD config/supervisor/ /app/data/supervisor/
RUN rm -rf /etc/supervisor/supervisord.conf && \
    rm -rf /etc/supervisor/conf.d/ && \
    ln -sf /app/data/supervisor/supervisord.conf /etc/supervisor/supervisord.conf && \
    ln -sf /app/data/supervisor/conf.d /etc/supervisor/conf.d && \
    ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

#USER openhabcloud

#CMD ["node" "app.js"]

COPY start.sh /app/code/
CMD [ "/app/code/start.sh" ]
