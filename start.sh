#!/bin/bash

OPENHAB_DIR="/app/data/openhab"
OPENHAB_CONF="${OPENHAB_DIR}/config.json"

log() {
   echo "DEBUG:" "$*"
}

create_config() {
   log "Creating ${OPENHAB_CONF}..."

# check all config options in config-production.json

#
# "apn" : {
#      "team": "PB1234567",
#      "keyId": "BLABLA1",
#      "defaultTopic": "es.spaphone.openhab",
#      "signingKey": "certs/aps/AuthKey.p8"
#    },
#
#    "mail": {
#      "host" : "smtp",
#      "port" : 465,
#      "user" : "foo@bar.com",
#      "pass" : "password"
#    },
#
#    "mailer": {
#        "host" : "smtp",
#        "port": 465,
#        "secureConnection": true,
#        "user": "foo@bar.com",
#        "password": "password",
#        "from": "openHAB Cloud <your@email.address>"
#    },

   cat <<EOF >> ${OPENHAB_CONF}
{
    "system": {
      "host": "${CLOUDRON_APP_DOMAIN}",
      "port": "443",
      "protocol": "https",
      "logger" : {
        "dir": "./logs",
        "maxFiles" : "7d",
        "level" : "debug",
        "morganOption" : null
      },
      "subDomainCookies": false,
      "muteNotifications": false
    },
    "express":{
      "key" : "some express key"
    },
    "gcm" : {
      "jid": "something@gcm.googleapis.com",
      "password": "password"
    },
    "ifttt" : {
      "iftttChannelKey" : "key",
      "iftttTestToken" : "token"
    },
    "mongodb": {
        "hosts": ["${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT}"],
        "db": "${CLOUDRON_MONGODB_DATABASE}",
        "user": "${CLOUDRON_MONGODB_USERNAME}",
        "password": "${CLOUDRON_MONGODB_PASSWORD}"
    },
    "redis": {
        "host": "${CLOUDRON_REDIS_HOST}",
        "port": "${CLOUDRON_REDIS_PORT}",
        "password": "${CLOUDRON_REDIS_PASSWORD}"
    },
    "mailer": {
        "host" : "${CLOUDRON_MAIL_SMTP_SERVER}",
        "port": ${CLOUDRON_MAIL_SMTP_PORT},
        "secureConnection": true,
        "user": "${CLOUDRON_MAIL_SMTP_USERNAME}",
        "password": "${CLOUDRON_MAIL_SMTP_PASSWORD}",
        "from": "openHab Cloud <${CLOUDRON_MAIL_FROM}>"
    },
    "legal": {
    	"terms" : "",
        "policy": ""
    },
    "registration_enabled": true
}
EOF
}

if [[ ! -f "${OPENHAB_CONF}" ]]; then
   create_config
fi
echo "Starting openHAB"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i openHAB
